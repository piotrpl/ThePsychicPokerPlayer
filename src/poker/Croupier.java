package poker;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import poker.PokerRulesTable.Hand;

/**
 * @author Piotr Krzepczak
 * 
 * Represents a card dealer. Handles reading/writing files and card sets
 * processing.
 * 
 */
public class Croupier {
	private static final String OUTPUT_HAND = "Hand: ";
	private static final String OUTPUT_DECK = "Deck: ";
	private static final String OUTPUT_BEST_HAND = "Best Hand: ";
	private static final String SEPARATOR = " ";
	private static final String INPUT_FILE_NAME = "input.txt";
	private static final String OUTPU_FILE_NAME = "output.txt";
	private List<String> rounds;

	/**
	 * Reads in content of the input file
	 * 
	 * @return List<String> list of lines from the input file
	 */
	public List<String> startGame() {
		rounds = new ArrayList<String>();
		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(INPUT_FILE_NAME));
			String round = null;
			while ((round = bufferedReader.readLine()) != null) {
				rounds.add(round);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// Close the BufferedReader
			try {
				if (bufferedReader != null) {
					bufferedReader.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return rounds;
	}

	/**
	 * Writes the game result to the file.
	 * 
	 * @param results
	 *            list of lines to write into file
	 */
	public void endGame(List<String> results) {
		BufferedWriter bufferedWriter = null;
		try {
			bufferedWriter = new BufferedWriter(new FileWriter(OUTPU_FILE_NAME));
			for (String roundResult : results) {
				bufferedWriter.write(roundResult);
				bufferedWriter.newLine();
			}
			bufferedWriter.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// Close the BufferedWriter
			try {
				if (bufferedWriter != null) {
					bufferedWriter.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}

	/**
	 * Initiates card sets for each line from the input file
	 * 
	 * @param round
	 *            line from the input file
	 * @param handCards
	 *            set of first 5 cards
	 * @param deckCards
	 *            set of last 5 cards
	 */
	public void dealCards(String round, List<Card> handCards,
			List<Card> deckCards) {
		String[] cards = round.split(SEPARATOR);
		for (int i = 0; i < cards.length; i++) {
			if (i < 5) {
				handCards.add(new Card(cards[i].charAt(0), cards[i].charAt(1)));
			} else {
				deckCards.add(new Card(cards[i].charAt(0), cards[i].charAt(1)));
			}
		}
	}

	/**
	 * Creates the output line for particular card sets
	 * 
	 * @param score
	 *            best hand
	 * @param handCards
	 *            set of first 5 cards
	 * @param deckCards
	 *            set of last 5 cards
	 * @return String the output line
	 */
	public String callRoundWinner(Hand score, List<Card> handCards,
			List<Card> deckCards) {
		String outputLine = OUTPUT_HAND;
		for (Card card : handCards) {
			outputLine += card + SEPARATOR;
		}

		outputLine += OUTPUT_DECK;
		for (Card card : deckCards) {
			outputLine += card + SEPARATOR;
		}

		outputLine += OUTPUT_BEST_HAND + score.type();
		return outputLine;
	}
}
