package poker;

import java.util.ArrayList;
import java.util.List;

import poker.PokerRulesTable.Hand;

/**
 * @author Piotr Krzepczak
 * 
 * Represents Poker game. Handles execution order.
 */
public class Game {
	private List<String> rounds;
	private List<Card> handCards;
	private List<Card> deckCards;
	private List<String> roundsResults = new ArrayList<String>();
	private Croupier croupier = new Croupier();
	private Gambler gambler = new Gambler();

	/**
	 * Runs the Poker game.
	 */
	public void play() {
		// read in file content
		rounds = croupier.startGame();
		// for each line in file
		for (String round : rounds) {
			handCards = new ArrayList<Card>();
			deckCards = new ArrayList<Card>();
			// create card sets
			croupier.dealCards(round, handCards, deckCards);
			// find the highest hand
			Hand score = gambler.playCards(handCards, deckCards);
			// create the output line
			roundsResults.add(croupier.callRoundWinner(score, handCards,
					deckCards));
			// reset highest hand before new line
			gambler.clearMind();
		}
		// write result to file
		croupier.endGame(roundsResults);
	}
}
