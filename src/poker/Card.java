package poker;

/**
 * @author Piotr Krzepczak
 * 
 * Represents a card from a card deck. Implements
 * <code>Comperable</code> to order cards in any deck by using their
 * numeric value.
 */
public class Card implements Comparable<Card> {
	private char faceValue;
	private char suitCharacter;
	private int faceNumericValue;

	public Card(char faceValue, char suitCharacter) {
		this.faceValue = faceValue;
		this.suitCharacter = suitCharacter;

		// assign numeric value
		switch (faceValue) {
		case '2':
			faceNumericValue = 0;
			break;
		case '3':
			faceNumericValue = 1;
			break;
		case '4':
			faceNumericValue = 2;
			break;
		case '5':
			faceNumericValue = 3;
			break;
		case '6':
			faceNumericValue = 4;
			break;
		case '7':
			faceNumericValue = 5;
			break;
		case '8':
			faceNumericValue = 6;
			break;
		case '9':
			faceNumericValue = 7;
			break;
		case 'T':
			faceNumericValue = 8;
			break;
		case 'J':
			faceNumericValue = 9;
			break;
		case 'Q':
			faceNumericValue = 10;
			break;
		case 'K':
			faceNumericValue = 11;
			break;
		case 'A':
			faceNumericValue = 12;
			break;

		default:
			break;
		}
	}

	public char getFaceValue() {
		return faceValue;
	}

	public char getSuitCharacter() {
		return suitCharacter;
	}

	public int getFaceNumericValue() {
		return faceNumericValue;
	}

	@Override
	public int compareTo(Card o) {
		return new Integer(this.faceNumericValue).compareTo(new Integer(
				o.faceNumericValue));
	}

	@Override
	public String toString() {
		return Character.toString(faceValue) + suitCharacter;
	}
}
