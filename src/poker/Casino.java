package poker;

/**
 * @author Piotr Krzepczak
 * 
 * Main class and entry point for the execution.
 */
public class Casino {

	/**
	 * Entry method.
	 * 
	 * @param args
	 *            arguments
	 */
	public static void main(String[] args) {
		Game game = new Game();
		game.play();
	}

}
