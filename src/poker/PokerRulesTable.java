package poker;

import java.util.List;

/**
 * @author Piotr Krzepczak
 * 
 * Encapsulates Poker specific values and checks.
 * 
 */
public final class PokerRulesTable {

	/**
	 * Represents Poker hands and correlated numeric values.
	 */
	public enum Hand {
		STRAIGHT_FLUSH("straight-flush", 0), 
		FOUR_OF_A_KIND("four-of-a-kind", 1), 
		FULL_HOUSE("full-house", 2), 
		FLUSH("flush", 3), 
		STRAIGHT("straight", 4), 
		THREE_OF_A_KIND("three-of-a-kind", 5), 
		TWO_PAIRS("two-pairs", 6), 
		ONE_PAIR("one-pair", 7), 
		HIGHEST_CARD("highest-card", 8);

		private final String type;
		private final int value;

		Hand(String type, int value) {
			this.type = type;
			this.value = value;
		}

		public String type() {
			return type;
		}

		public int value() {
			return value;
		}
	}

	/**
	 * Evaluates if particular card set is a straight-flush hand.
	 * 
	 * @param cards
	 *            card set
	 * @return true if straight-flush, false otherwise
	 */
	public static boolean isStraightFlush(List<Card> cards) {
		if (isFlush(cards) && isStraight(cards)) {
			return true;
		}
		return false;
	}

	/**
	 * Evaluates if particular card set is a four-of-a-kind hand.
	 * 
	 * @param cards
	 *            card set
	 * @return true if four-of-a-kind, false otherwise
	 */
	public static boolean isFourOfAKind(List<Card> cards) {
		for (int i = 0; i < cards.size() - 3; i++) {
			if (cards.get(i).getFaceNumericValue() == cards.get(i + 1)
					.getFaceNumericValue()
					&& cards.get(i + 1).getFaceNumericValue() == cards.get(i + 2)
							.getFaceNumericValue()
					&& cards.get(i + 2).getFaceNumericValue() == cards.get(i + 3)
							.getFaceNumericValue()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Evaluates if particular card set is a full-house hand.
	 * 
	 * @param cards
	 *            card set
	 * @return true if full-house, false otherwise
	 */
	public static boolean isFullHouse(List<Card> cards) {
		if (cards.get(0).getFaceNumericValue() == cards.get(1)
				.getFaceNumericValue()
				&& cards.get(1).getFaceNumericValue() == cards.get(2)
						.getFaceNumericValue()) {
			if (cards.get(3).getFaceNumericValue() == cards.get(4)
					.getFaceNumericValue()) {
				return true;
			}
		}

		if (cards.get(0).getFaceNumericValue() == cards.get(1)
				.getFaceNumericValue()) {
			if (cards.get(2).getFaceNumericValue() == cards.get(3)
					.getFaceNumericValue()
					&& cards.get(3).getFaceNumericValue() == cards.get(4)
							.getFaceNumericValue()) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Evaluates if particular card set is a flush hand.
	 * 
	 * @param cards
	 *            card set
	 * @return true if flush, false otherwise
	 */
	public static boolean isFlush(List<Card> cards) {
		for (int i = 0; i < cards.size() - 1; i++) {
			if (cards.get(i).getSuitCharacter() != cards.get(i + 1)
					.getSuitCharacter()) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Evaluates if particular card set is a straight hand.
	 * 
	 * @param cards
	 *            card set
	 * @return true if straight, false otherwise
	 */
	public static boolean isStraight(List<Card> cards) {
		if ((cards.get(1).getFaceNumericValue() == (cards.get(0)
				.getFaceNumericValue() + 1) % 13)
				&& (cards.get(2).getFaceNumericValue() == (cards.get(0)
						.getFaceNumericValue() + 2) % 13)
				&& (cards.get(3).getFaceNumericValue() == (cards.get(0)
						.getFaceNumericValue() + 3) % 13)
				&& (cards.get(4).getFaceNumericValue() == (cards.get(0)
						.getFaceNumericValue() + 4) % 13)) {
			return true;
		}
		if ((cards.get(4).getFaceNumericValue() == 12)
				&& (cards.get(0).getFaceNumericValue() == 0)
				&& (cards.get(1).getFaceNumericValue() == 1)
				&& (cards.get(2).getFaceNumericValue() == 2)
				&& (cards.get(3).getFaceNumericValue() == 3)) {
			return true;
		}
		return false;
	}

	/**
	 * Evaluates if particular card set is a three-of-a-kind hand.
	 * 
	 * @param cards
	 *            card set
	 * @return true if three-of-a-kind, false otherwise
	 */
	public static boolean isThreeOfAKind(List<Card> cards) {
		for (int i = 0; i < cards.size() - 2; i++) {
			if (cards.get(i).getFaceNumericValue() == cards.get(i + 1)
					.getFaceNumericValue()
					&& cards.get(i + 1).getFaceNumericValue() == cards.get(i + 2)
							.getFaceNumericValue()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Evaluates if particular card set is a two-pairs hand.
	 * 
	 * @param cards
	 *            card set
	 * @return true if two-pairs, false otherwise
	 */
	public static boolean isTwoPairs(List<Card> cards) {
		for (int i = 0; i < cards.size() - 1; i++) {
			if (cards.get(i).getFaceNumericValue() == cards.get(i + 1)
					.getFaceNumericValue()) {
				for (int j = i + 2; j < cards.size() - 1; j++) {
					if (cards.get(j).getFaceNumericValue() == cards.get(j + 1)
							.getFaceNumericValue()) {
						return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Evaluates if particular card set is a one-pair hand.
	 * 
	 * @param cards
	 *            card set
	 * @return true if one-pair, false otherwise
	 */
	public static boolean isOnePair(List<Card> cards) {
		for (int i = 0; i < cards.size() - 1; i++) {
			if (cards.get(i).getFaceNumericValue() == cards.get(i + 1)
					.getFaceNumericValue()) {
				return true;
			}
		}
		return false;
	}
}
