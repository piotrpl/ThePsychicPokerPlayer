package poker;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import poker.PokerRulesTable.Hand;

/**
 * @author Piotr Krzepczak
 * 
 * Represents a Poker game player. Contains logic for finding the best
 * hand.
 * 
 */
public class Gambler {
	private Hand bestHand = Hand.HIGHEST_CARD;

	/**
	 * Finds the best hand from the provided sets of cards
	 * 
	 * @param handCards
	 *            list of 5 first cards
	 * @param deckCards
	 *            list of 5 last cards
	 * @return
	 */
	public Hand playCards(List<Card> handCards, List<Card> deckCards) {
		// check score for the current hand
		checkForBestHand(handCards.toArray(new Card[handCards.size()]));

		// take one by one cards from the deck
		Card[] currentHand = new Card[5];
		for (int i = 0; i < 5; i++) {
			currentHand[i] = new Card(deckCards.get(i).getFaceValue(),
					deckCards.get(i).getSuitCharacter());
			// use recursion to match cards from the deck and cards from the
			// hand
			matchWithHandCards(currentHand, handCards, i + 1, 0);
		}

		return bestHand;
	}

	/**
	 * Fills in recursively the deck of cards for best hand verification
	 * 
	 * @param currentHand
	 *            array of cards for the best hand check
	 * @param handCards
	 *            list of the hand cards
	 * @param currentIdx
	 *            currently processed index
	 * @param startIdx
	 *            index from which to start processing
	 */
	private void matchWithHandCards(Card[] currentHand, List<Card> handCards,
			int currentIdx, int startIdx) {
		// there are already 5 cards to check
		if (currentIdx == 5) {
			checkForBestHand(currentHand);
			return;
		}

		for (int i = startIdx; i < 5; i++) {
			currentHand[currentIdx] = handCards.get(i);
			matchWithHandCards(currentHand, handCards, currentIdx + 1, i + 1);
		}
	}

	/**
	 * Verify if particular card set is the best hand
	 * 
	 * @param currentHand
	 */
	private void checkForBestHand(Card[] currentHand) {
		List<Card> cardsToCheck = new ArrayList<Card>();
		for (Card card : currentHand) {
			cardsToCheck.add(card);
		}
		// sort so that we could easily compare cards for hand type
		Collections.sort(cardsToCheck);

		// verify what hand is that
		if (PokerRulesTable.isStraightFlush(cardsToCheck)) {
			if (bestHand.value() > Hand.STRAIGHT_FLUSH.value()) {
				bestHand = Hand.STRAIGHT_FLUSH;
			}
		} else if (PokerRulesTable.isFourOfAKind(cardsToCheck)) {
			if (bestHand.value() > Hand.FOUR_OF_A_KIND.value()) {
				bestHand = Hand.FOUR_OF_A_KIND;
			}
		} else if (PokerRulesTable.isFullHouse(cardsToCheck)) {
			if (bestHand.value() > Hand.FULL_HOUSE.value()) {
				bestHand = Hand.FULL_HOUSE;
			}
		} else if (PokerRulesTable.isFlush(cardsToCheck)) {
			if (bestHand.value() > Hand.FLUSH.value()) {
				bestHand = Hand.FLUSH;
			}
		} else if (PokerRulesTable.isStraight(cardsToCheck)) {
			if (bestHand.value() > Hand.STRAIGHT.value()) {
				bestHand = Hand.STRAIGHT;
			}
		} else if (PokerRulesTable.isThreeOfAKind(cardsToCheck)) {
			if (bestHand.value() > Hand.THREE_OF_A_KIND.value()) {
				bestHand = Hand.THREE_OF_A_KIND;
			}
		} else if (PokerRulesTable.isTwoPairs(cardsToCheck)) {
			if (bestHand.value() > Hand.TWO_PAIRS.value()) {
				bestHand = Hand.TWO_PAIRS;
			}
		} else if (PokerRulesTable.isOnePair(cardsToCheck)) {
			if (bestHand.value() > Hand.ONE_PAIR.value()) {
				bestHand = Hand.ONE_PAIR;
			}
		}
		return;
	}

	/**
	 * Resets best hand value.
	 * 
	 */
	public void clearMind() {
		bestHand = Hand.HIGHEST_CARD;
	}
}
